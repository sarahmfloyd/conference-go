import requests
from .keys import PEXEL_API
import json


def get_city_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}, {state}"
    headers = {"Authorization": PEXEL_API}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    return data["photos"][0]['src']['original']
