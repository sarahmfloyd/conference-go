from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from events.api_views import ConferenceEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference


class PresentationEncoder(ModelEncoder):
    model = Presentation
    properties = ["title", "status"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = ["presenter_name", "company_name", "presenter_email",
                  "title", "synopsis", "created", "conference"]
    encoders = {"conference": ConferenceEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse({"presentations": presentations},
                            encoder=PresentationEncoder, safe=False)
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conferences = Conference.objects.get(id=content["conference"])
                content["conference"] = conferences
        except Conference.DoesNotExist:
            return JsonResponse({"message": "conference invalid"},
                                status=400)
        presentation = Presentation.create(**content)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder,
                            safe=False)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(presentation, encoder=PresentationDetailEncoder,
                            safe=False)
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=content["conference"])
            content["conference"] = conference
        except Presentation.DoesNotExist:
            return JsonResponse({
                "message": "invalid conference id"}, status=400
            )
    Presentation.objects.filter(id=id).update(**content)
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(presentation, encoder=PresentationDetailEncoder,
                        safe=False)
